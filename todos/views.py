from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem
# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.pk})


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.pk})


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            'todo_list_detail',
            kwargs={'pk': self.object.list.pk},
            )

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["lists"] = TodoList.objects.all()
    #     return context


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            'todo_list_detail',
            kwargs={'pk': self.object.list.pk},
            )
